FROM openjdk:17-jdk

COPY target/fmass-0.0.1-SNAPSHOT.jar /app/back-fish.jar

EXPOSE 8585

CMD ["java", "-jar", "/app/back-fish.jar"]
